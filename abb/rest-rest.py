# -*- coding: utf-8 -*-
"""Connector between the ABB EQmatic Energy Analyzer and The Green Village digital platform.

The ABB module is read out by REST API calls which is less performant than Modbus.
The data is sent to the Kafka REST Proxy which is less performant than using a Kafka producer.
"""

import logging
import os
import pathlib
import time
from configparser import ConfigParser

import requests
from requests.exceptions import RequestException

logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
logger = logging.getLogger(__file__)

parent_dir = pathlib.Path(__file__).parent.resolve()

# Read config file.
config = ConfigParser()
config.read(str(parent_dir / "pretaloger.cfg"))
abb_host = config.get("ABB", "host")
abb_token = config.get("ABB", "token")
rest_url = config.get("RestProxy", "url")
rest_username = config.get("RestProxy", "username")
rest_password = config.get("RestProxy", "password")
registry_url = config.get("RestProxy", "schema_registry_url")

# Time between sensor readouts in seconds
period = int(os.getenv("PERIOD", 1))
logger.info("Period between data readout: %d seconds" % (period))

# Get schema from the schema registry.
r = requests.get(registry_url, timeout=5)
logger.debug(r.text)
# logger.info(f"Kafka Schema Registry response code: {r.status_code}")
logger.info("Kafka Schema Registry response code: %d" % (r.status_code))
# value_schema_id = r.json()['id']
schema = r.json()["schema"]

# Get the list of devices.
# The following response is expected:
# [
#   {
#     "fingerPrint": 1,
#     "primaryAddress": 1,
#     "serialNumber": "256177",
#     "manufacturer": "ABB",
#     "configStatus": true,
#     "status": "OK",
#     "medium": "Electricity",
#     "version": 768,
#     "baudrate": 9600
#   },
#   {
#     "fingerPrint": 2,
#     "primaryAddress": 2,
#     "serialNumber": "39164",
#     "manufacturer": "ABB",
#     "configStatus": true,
#     "status": "OK",
#     "medium": "Electricity",
#     "version": 768,
#     "baudrate": 9600
#   },
#   {
#     "fingerPrint": 3,
#     "primaryAddress": 3,
#     "serialNumber": "39203",
#     "manufacturer": "ABB",
#     "configStatus": true,
#     "status": "OK",
#     "medium": "Electricity",
#     "version": 768,
#     "baudrate": 9600
#   }
# ]
# r = requests.get(f"{abb_host}/api/meters/list",
#                  headers={"Authorization": f"Longterm {abb_token}"},
r = requests.get(
    "%s/api/meters/list" % (abb_host),
    headers={"Authorization": "Longterm %s" % (abb_token)},
    timeout=5,
)
logger.debug(r.text)
# logger.info(f"ABB /api/meters/list response code: {r.status_code}")
logger.info("ABB /api/meters/list response code: %s" % (r.status_code))
devices = r.json()

# Get the list of possible data points for each device.
# The ABB REST API returns the data points in the following format:
# {
#   "unit": "V",
#   "multiplier": 0.1,
#   "coding": 4,
#   "size": 2,
#   "msb": 1,
#   "msw": 0,
#   "recordNumber": 219904,
#   "valueTypeDescription": "Voltage  phase 1",
#   "valueTypeInfo": {
#     "direction": null,
#     "type": "Voltage",
#     "subType": null,
#     "subTypeSuffix": null,
#     "module": "Phase 1",
#     "group": "Instantaneous",
#     "tariff": null
#   }
# }
device_datapoints = []
for d in devices:
    # r = requests.get(f"{abb_host}/api/meters/{d['fingerPrint']}/dataPoints/list",
    #                  headers={"Authorization": f"Longterm {abb_token}"},
    r = requests.get(
        "%s/api/meters/%d/dataPoints/list" % (abb_host, d["fingerPrint"]),
        headers={"Authorization": "Longterm %s" % (abb_token)},
        timeout=5,
    )
    logger.debug(r.text)
    # logger.info(f"ABB /api/meters/{d['fingerPrint']}/dataPoints/list response code: {r.status_code}")
    logger.info(
        "ABB /api/meters/%s/dataPoints/list response code: %s"
        % (d["fingerPrint"], r.status_code)
    )
    device_datapoints.append({"fingerPrint": d["fingerPrint"], "dataPoints": r.json()})

# Selection of quantities to send to The Green Village data platform.
# The ABB REST API returns all possible measurements for each device.
# Only the ones for which valueTypeDescription is in the valueTypesDescriptions
# below will be forwarded.
device_measurements = [
    {
        "fingerPrint": 1,
        "device_id": "PV_carport",
        "valueTypeDescriptions": [
            "Energy active exported total",
            "Energy active net total",
            "Energy reactive imported total",
            "Energy reactive exported total",
            "Energy reactive net total",
            "Energy apparent imported total",
            "Energy apparent exported total",
            "Energy apparent net total",
            "Energy active imported phase 1",
            "Energy active imported phase 2",
            "Energy active imported phase 3",
            "Energy active exported phase 1",
            "Energy active exported phase 2",
            "Energy active exported phase 3",
            "Energy active net phase 1",
            "Energy active net phase 2",
            "Energy active net phase 3",
            "Energy reactive imported phase 1",
            "Energy reactive imported phase 2",
            "Energy reactive imported phase 3",
            "Energy reactive exported phase 1",
            "Energy reactive exported phase 2",
            "Energy reactive exported phase 3",
            "Energy reactive net phase 1",
            "Energy reactive net phase 2",
            "Energy reactive net phase 3",
            "Energy apparent imported phase 2",
            "Energy apparent imported phase 3",
            "Energy apparent exported phase 1",
            "Energy apparent exported phase 2",
            "Energy apparent exported phase 3",
            "Energy apparent net phase 1",
            "Energy apparent net phase 2",
            "Energy apparent net phase 3",
            "Voltage  phase 1",
            "Voltage  phase 2",
            "Voltage  phase 3",
            "Voltage  phase 1-2",
            "Voltage  phase 2-3",
            "Voltage  phase 3-1",
            "Current  phase 1",
            "Current  phase 2",
            "Current  phase 3",
            "Power active imported total",
            "Power active imported phase 1",
            "Power active imported phase 2",
            "Power active imported phase 3",
            "Power reactive imported total",
            "Power reactive imported phase 1",
            "Power reactive imported phase 2",
            "Power reactive imported phase 3",
            "Power apparent imported total",
            "Power apparent imported phase 1",
            "Power apparent imported phase 2",
            "Power apparent imported phase 3",
        ],
    },
    {
        "fingerPrint": 2,
        "device_id": "kWh_meter_1",
        "valueTypeDescriptions": [
            "Current  phase 1",
            "Power active imported total",
            "Energy active imported total",
            "Voltage  phase 1",
        ],
    },
    {
        "fingerPrint": 3,
        "device_id": "kWh_meter_2",
        "valueTypeDescriptions": [
            "Current  phase 1",
            "Power active imported total",
            "Energy active imported total",
            "Voltage  phase 1",
        ],
    },
]

while True:
    records = []
    for d in devices:
        # Read out values.
        # The REST API returns values in the following format:
        # {
        #   "recordNumber": 219904,
        #   "value": 2359
        # }
        try:
            # r = requests.get(f"{abb_host}/api/meters/{d['fingerPrint']}/values",
            #                  headers={"Authorization": f"Longterm {abb_token}"},
            r = requests.get(
                "%s/api/meters/%d/values" % (abb_host, d["fingerPrint"]),
                headers={"Authorization": "Longterm %s" % (abb_token)},
                timeout=5,
            )
            logger.debug(r.text)
            # logger.info(f"ABB /api/meters/{d['fingerPrint']}/values response code: {r.status_code}")
            logger.info(
                "ABB /api/meters/%s/values response code: %s"
                % (d["fingerPrint"], r.status_code)
            )
            if r.status_code != 200:
                continue
            readings = r.json()["values"]

        except RequestException as e:
            logger.error(e)
            continue

        values = []
        # datapoints = next(filter(lambda x: x['fingerPrint'] == d['fingerPrint'], device_datapoints))
        # measurements = next(filter(lambda x: x['fingerPrint'] == d['fingerPrint'], device_measurements))
        datapoints = [
            x for x in device_datapoints if x["fingerPrint"] == d["fingerPrint"]
        ][0]
        measurements = [
            x for x in device_measurements if x["fingerPrint"] == d["fingerPrint"]
        ][0]
        for r in readings:
            # datapoint = next(filter(lambda x: x['recordNumber'] == r['recordNumber'], datapoints['dataPoints']))
            datapoint = [
                x
                for x in datapoints["dataPoints"]
                if x["recordNumber"] == r["recordNumber"]
            ][0]
            if (
                not datapoint["valueTypeDescription"]
                in measurements["valueTypeDescriptions"]
            ):
                continue

            value = {
                "name": datapoint["valueTypeDescription"],
                "description": datapoint["valueTypeDescription"],
                "unit": datapoint["unit"],
                "type": "FLOAT",
                "value": {"float": r["value"] * datapoint["multiplier"]},
            }
            logger.debug(value)
            values.append(value)

        value = {
            "value": {
                "metadata": {
                    "project_id": "pretaloger",
                    "device_id": measurements["device_id"],
                    "description": measurements["device_id"],
                    "type": {"null": None},
                    "manufacturer": {"string": d["manufacturer"]},
                    "serial": {"string": d["serialNumber"]},
                    "placement_timestamp": {"null": None},
                    "location": {"null": None},
                    "latitude": {"null": None},
                    "longitude": {"null": None},
                    "altitude": {"null": None},
                },
                "data": {
                    "project_id": "pretaloger",
                    "device_id": measurements["device_id"],
                    "timestamp": int(time.time() * 1e3),  # time in ms
                    "values": values,
                },
            }
        }
        records.append(value)

    data = {
        # "value_schema_id": value_schema_id,
        "value_schema": schema,
        "records": records,
    }

    try:
        headers = {
            "Content-Type": "application/vnd.kafka.avro.v2+json",
            "Accept": "application/vnd.kafka.v2+json",
        }
        r = requests.post(
            rest_url,
            json=data,
            headers=headers,
            auth=(rest_username, rest_password),
            timeout=5,
        )
        logger.info(r.text)
        # logger.info(f"Kafka REST Proxy response code: {r.status_code}")
        logger.info("Kafka REST Proxy response code: %d" % (r.status_code))

    except RequestException as e:
        logger.error(e)
        continue

    time.sleep(period)
